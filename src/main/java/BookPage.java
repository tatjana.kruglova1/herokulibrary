import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BookPage extends PageObject {

    @FindBy(xpath="//h1")
    private WebElement bookTitle;

    public BookPage(WebDriver driver) {
        super(driver);
    }

    public boolean isInitialized() {
        return !driver.findElements(By.linkText("Delete Book")).isEmpty();
    }

    public String getBookTitle() {
        return bookTitle.getText().replace("Title: ", "");
    }

    public String getBookId() {
        String url = driver.getCurrentUrl();
        String[] splitUrl = url.split("book/");
        return splitUrl[1];
    }

    public boolean isFiction() {
        return !driver.findElements(By.linkText("Fiction")).isEmpty();
    }

    public boolean isNonfiction() {
        return !driver.findElements(By.linkText("Non-fiction")).isEmpty();
    }
}
