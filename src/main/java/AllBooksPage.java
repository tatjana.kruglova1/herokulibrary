import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AllBooksPage extends PageObject {

    @FindBy(xpath="//h1")
    private WebElement bookListHeader;

    public AllBooksPage(WebDriver driver) {
        super(driver);
    }

    public boolean isInitialized() {
        return bookListHeader.isDisplayed();
    }

    public boolean bookTitleIsShown(String bookTitle) {
        return driver.findElement(By.xpath("//*[contains(text(), '" + bookTitle + "')]")).isDisplayed();
    }

    public boolean bookLinkIsShown(String bookId) {
        return driver.findElement(By.xpath("//a[contains(@href,'" + bookId + "')]")).isDisplayed();
    }
}
