import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AddBookPage extends PageObject {

    @FindBy(id="title")
    private WebElement title;

    @FindBy(id="author")
    private WebElement author;

    @FindBy(id="summary")
    private WebElement summary;

    @FindBy(id="isbn")
    private WebElement isbn;

    @FindBy(id="5b6714c73809970014e31c99")
    private WebElement nonfiction;

    @FindBy(id="5b6714c93809970014e31c9a")
    private WebElement fiction;

    @FindBy(xpath="//*[contains(text(), 'Submit')]")
    private WebElement submitButton;

    public AddBookPage(WebDriver driver) {
        super(driver);
    }

    public boolean isInitialized() {
        return title.isDisplayed();
    }

    public void enterBookTitle(String bookTitle) {
        this.title.clear();
        this.title.sendKeys(bookTitle);
    }

    public void selectBookAuthor(String bookAuthor) {
        this.author.sendKeys(bookAuthor);
    }

    public void enterBookSummary(String bookSummary) {
        this.summary.clear();
        this.summary.sendKeys(bookSummary);
    }

    public void enterBookISBN(String bookISBN) {
        this.isbn.clear();
        this.isbn.sendKeys(bookISBN);
    }

    public void selectNonfiction(){
        nonfiction.click();
    }

    public void selectFiction(){
        fiction.click();
    }

    public BookPage submit(){
        submitButton.click();
        return new BookPage(driver);
    }

    public BookPage addNewBook(String title, String author, String summary, String isin, boolean fiction, boolean nonfiction) {
        this.enterBookTitle(title);
        this.selectBookAuthor(author);
        this.enterBookSummary(summary);
        this.enterBookISBN(isin);
        if (fiction) {
            this.selectFiction();
        }
        if (nonfiction) {
            this.selectNonfiction();
        }
        return this.submit();
    }

}
