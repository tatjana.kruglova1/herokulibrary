import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static junit.framework.Assert.assertTrue;


public class AllBooksTest extends BaseTest {

    AllBooksPage allBooksPage;
    static String bookId;
    static String bookTitle;

    @BeforeClass
    public static void setUp() {
        BaseTest.setUp();
        driver.get("https://raamatukogu.herokuapp.com/catalog/book/create");
        AddBookPage addBookPage = new AddBookPage(driver);
        BookPage bookPage = addBookPage.addNewBook("Book Title", "King, Stephen", "Testing Book", "0783324824353", false, false);
        bookId = bookPage.getBookId();
        bookTitle = bookPage.getBookTitle();
    }

    @Before
    public void beforeTest() {
        driver.get("https://raamatukogu.herokuapp.com/catalog/books");
        allBooksPage = new AllBooksPage(driver);
        assertTrue(allBooksPage.isInitialized());
    }

    @Test
    public void verifyAddedBookTest(){
        assertTrue(allBooksPage.bookTitleIsShown(bookTitle));
        assertTrue(allBooksPage.bookLinkIsShown(bookId));
    }

}
