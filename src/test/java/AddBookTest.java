import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.*;


public class AddBookTest extends BaseTest {

    AddBookPage addBookPage;

    @Before
    public void beforeTest() {
        driver.get("https://raamatukogu.herokuapp.com/catalog/book/create");
        addBookPage = new AddBookPage(driver);
        assertTrue(addBookPage.isInitialized());
    }

    @Test
    public void testAddBook(){
        addBookPage.enterBookTitle("Book Title");
        addBookPage.selectBookAuthor("King, Stephen");
        addBookPage.enterBookSummary("Testing Book");
        addBookPage.enterBookISBN("0783324824353");

        BookPage bookPage = addBookPage.submit();
        assertTrue(bookPage.isInitialized());
        assertEquals("Book Title", bookPage.getBookTitle());
    }

    @Test
    public void testAddBookWithoutTitle(){
        BookPage bookPage = addBookPage.addNewBook("", "King, Stephen", "Testing Book", "0783324824353", false, false);

        assertFalse(bookPage.isInitialized());
    }

    @Test
    public void testAddBookWithoutSummary(){
        BookPage bookPage = addBookPage.addNewBook("Book Title", "King, Stephen", "", "0783324824353", false, false);

        assertFalse(bookPage.isInitialized());
    }

    @Test
    public void testAddBookWithoutISIN(){
        BookPage bookPage = addBookPage.addNewBook("Book Title", "King, Stephen", "Testing Book", "", false, false);

        assertFalse(bookPage.isInitialized());
    }

    @Test
    public void testAddBookWithoutGenre() {
        BookPage bookPage = addBookPage.addNewBook("Book Title", "King, Stephen", "Testing Book", "0783324824353", false, false);
        assertTrue(bookPage.isInitialized());

        assertFalse(bookPage.isFiction());
        assertFalse(bookPage.isNonfiction());
    }

    @Test
    public void testAddFictionBook(){
        BookPage bookPage = addBookPage.addNewBook("Book Title", "King, Stephen", "Testing Book", "0783324824353", true, false);
        assertTrue(bookPage.isInitialized());

        assertTrue(bookPage.isFiction());
        assertFalse(bookPage.isNonfiction());
    }

    @Test
    public void testAddNonfictionBook(){
        BookPage bookPage = addBookPage.addNewBook("Book Title", "King, Stephen", "Testing Book", "0783324824353", false, true);
        assertTrue(bookPage.isInitialized());

        assertFalse(bookPage.isFiction());
        assertTrue(bookPage.isNonfiction());
    }

    @Test
    public void testAddFictionAndNonfictionBook(){
        BookPage bookPage = addBookPage.addNewBook("Book Title", "King, Stephen", "Testing Book", "0783324824353", true, true);
        assertTrue(bookPage.isInitialized());

        assertTrue(bookPage.isFiction());
        assertTrue(bookPage.isNonfiction());
    }
}
